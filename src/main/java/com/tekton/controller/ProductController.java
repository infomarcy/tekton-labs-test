package com.tekton.controller;


import com.tekton.model.product.ProductRequestDto;
import com.tekton.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.UUID;


@CrossOrigin
@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/api/v1/products")
public class ProductController {

   private final ProductService productService;

    @GetMapping(value ="/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public  ResponseEntity<?>  getProductById(@PathVariable("id") UUID id){
        return productService.getProductById(id);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> saveProduct(@Valid @RequestBody ProductRequestDto request) {
        return productService.saveProduct(request);
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateProduct(@Valid @RequestBody ProductRequestDto request, @RequestParam("id") UUID id) {
        return productService.updateProduct(request, id);
    }

}
