package com.tekton.model.product;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.Column;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class ProductResponseDto {
    private UUID id;
    private String name;
    private BigDecimal price;
    private String description;
    private String category;
    private String imageUrl;
    private int unitInStock;
    private String manufacturer;

}
