package com.tekton.model.product;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductRequestDto {

    @NotBlank (message= " The Product name is required")
    @ApiModelProperty(value = "name", example = "avocado")
    private String name;

    @Min(value = 0 , message =" The product price must not be less than 0")
    @ApiModelProperty(value = "price", example = "0.99")
    private BigDecimal price;

    @NotBlank (message= " The Product description is required")
    @ApiModelProperty(value = "description", example = "Fresh organic avocados from DR.")
    private String description;

    @NotBlank (message= " The Product category is required")
    @ApiModelProperty(value = "category", example = "Organic")
    private String category;

    @ApiModelProperty(value = "imageUrl", example = "https://static9.depositphotos.com/1642482/1148/i/950/depositphotos_11489971-stock-photo-avocado-cut-in-half.jpg")
    private String imageUrl;

    @Min(value = 0 , message =" The unitInStock must not be less than 0")
    @ApiModelProperty(value = "unitInStock", example = "48")
    private int unitInStock;

    @ApiModelProperty(value = "manufacturer", example = "AVOCADOS RD")
    private String manufacturer;

}
