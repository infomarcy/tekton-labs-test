package com.tekton.model.error;


import java.text.MessageFormat;

public enum CustomErrorCode {

        VALIDATION_ERROR("7003", "Validation error"),
        BINDING_ERROR_CODE("7004", "Binding error"),
        UNKNOWN_ERROR("7005", "An unknown error has occurred: %s"),
        RESOURCE_NOT_FOUND("7006", "No information was found for the query made.");


        private String code;
        private String description;

        private CustomErrorCode(String code, String description) {
            this.setCode(code);
            this.setDescription(description);
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getDescription() {
            return description;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getCode() {
            return code;
        }

        /**
         * Apply arg substitution to the Description.
         *
         * @param args arguments to be substituted into the description.
         */

        public void formatDescription(Object... args) {
            description = MessageFormat.format(description, args);
        }
    }
