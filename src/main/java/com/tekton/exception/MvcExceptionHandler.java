package com.tekton.exception;


import com.tekton.model.error.CustomErrorCode;
import com.tekton.model.error.ResponseError;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class MvcExceptionHandler {

    @ExceptionHandler({ConstraintViolationException.class})
    public ResponseEntity<String> validationErrorHandler(ConstraintViolationException e)  {
        ResponseError responseError = new ResponseError();
        responseError.setError_code(CustomErrorCode.VALIDATION_ERROR.getCode());

        List<String> errors = new ArrayList<>();
        errors.add(e.getMessage());
        responseError.setErrors(errors);
        return new ResponseEntity<>(responseError.toString(), HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler(BindException.class)
    public ResponseEntity<List> handleBindException(BindException ex) {
        ResponseError responseError = new ResponseError();
        responseError.setError_code(CustomErrorCode.BINDING_ERROR_CODE.getCode());

        List<String> errors = new ArrayList<>();
        ex.getAllErrors().forEach(err -> {
            errors.add(err.getDefaultMessage());
        });

        responseError.setErrors(errors);
        return new ResponseEntity(responseError.toString(), HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler({ Exception.class})
    public ResponseEntity<String> generalException(Exception e)  {
        ResponseError responseError = new ResponseError();
        responseError.setError_code(CustomErrorCode.UNKNOWN_ERROR.getCode());

        List<String> errors = new ArrayList<>();
        errors.add(e.getMessage());

        responseError.setErrors(errors);
        return new ResponseEntity<>(responseError.toString(), HttpStatus.BAD_REQUEST);
    }
}
