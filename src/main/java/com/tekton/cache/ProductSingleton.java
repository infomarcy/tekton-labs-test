package com.tekton.cache;

import com.tekton.dao.ProductDao;
import com.tekton.model.error.CustomErrorCode;
import com.tekton.model.product.ProductResponseDto;
import com.tekton.util.EntityMapper;
import com.tekton.util.ServiceUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Slf4j
public class ProductSingleton {


    private List<ProductResponseDto> productList;


    private ProductSingleton() {
        productList = new ArrayList<>();
    }

    private static ProductSingleton instance;

    public static ProductSingleton getInstance() {
        if (instance == null) {
            instance = new ProductSingleton();
        }
        return instance;
    }

    public void saveProductToCache(ProductResponseDto product) {
        try {
            if (product != null && product.getId() != null) {
                productList.remove(product);
                productList.add(product);
            }
        } catch (Exception e) {
            log.error("error: {}", e.getMessage());
        }
    }

    public ResponseEntity<?> getProductById(UUID id, ProductDao productDao, EntityMapper entityMapper) {
        try {
            // search from the in memory value
            Optional<ProductResponseDto> productResponseDto = Optional.ofNullable(findProductInCache(id));

            if (productResponseDto.isPresent()) {
                return new ResponseEntity<>(productResponseDto.get(), HttpStatus.OK);
            } else {
                // Get a product by Id
                return ServiceUtils.getResponseEntity(id, productDao, entityMapper);
            }
        } catch (Exception e) {
            log.error("" + e);
            return ServiceUtils.getResponseError(String.format(CustomErrorCode.UNKNOWN_ERROR.getDescription(), e.getMessage()), CustomErrorCode.UNKNOWN_ERROR.getCode(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ProductResponseDto findProductInCache(UUID id) {
        return productList.stream()
                .filter(p -> id.equals(p.getId()))
                .findAny()
                .orElse(null);
    }

    public boolean isProductInCache(UUID id) {
        if (findProductInCache(id) != null) {
            return true;
        }
        return false;
    }
}
