package com.tekton.aop;


import com.tekton.util.ApiConstant;
import com.tekton.util.CommonUtility;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class LoggerAspect {
    private static final Logger logger = LoggerFactory.getLogger(LoggerAspect.class);

    // Any class in a subpackage dao of com.tekton
    @Pointcut("execution(* com.tekton..dao.*.*(..))")
    public void Repository() {
    }

    @Around("Repository()")
    public Object log (ProceedingJoinPoint thisJointPoint) throws Throwable{
        // Get the name of the method
        String methodName = thisJointPoint.getSignature().getName();
        // Intercept and start counting the time
        long startTime =System.currentTimeMillis();
        // execute the original method
        Object result = thisJointPoint.proceed();
        // Log Response time
        String msj = "METHOD: ".concat(methodName).concat(" , Response Time: ").concat(String.valueOf((System.currentTimeMillis() - startTime)).concat(" ms, Date: ").concat(CommonUtility.getDate(ApiConstant.INTERNAL_DATE_TIME_FORMAT)));
        CommonUtility.monitorResponseTime(msj);
        // return the result
        return result;
    }


}
