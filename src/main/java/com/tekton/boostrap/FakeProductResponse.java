package com.tekton.boostrap;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class FakeProductResponse {
    private int id;
    private String title;
    private BigDecimal price;
    private String description;
    private String category;
    private String image;
    @JsonIgnore
    private Object rating;
}
