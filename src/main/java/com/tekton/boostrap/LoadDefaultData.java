package com.tekton.boostrap;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tekton.dao.CallApiDao;
import com.tekton.dao.ProductDao;
import com.tekton.domain.Product;
import com.tekton.model.product.ProductRequestDto;
import com.tekton.util.ApiConstant;
import com.tekton.util.CommonUtility;
import com.tekton.util.EntityMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Component
public class LoadDefaultData implements CommandLineRunner {


    private final ProductDao productDao;
    private final CallApiDao callApiDao;
    private final EntityMapper entityMapper;

    @Override
    public void run(String... args) {
        if (productDao.findAll().isEmpty()) {
            loadDefaultData();
        }
    }

    private void loadDefaultData() {
        try {
            //CALL API
            ResponseEntity<String> response = callApiDao.callApi(ApiConstant.API_FAKE_PRODUCT_URL);
            if (response != null) {
                JsonNode jsonNode = new ObjectMapper().readValue(response.getBody(), JsonNode.class);

                List<FakeProductResponse> fakeProductResponse = CommonUtility.fromJSON(FakeProductResponse.class, jsonNode.toString());

                //Load data to our Database
                loadProductToDb(fakeProductResponse);
            }
        } catch (Exception e) {
            log.error("error: {}", e.getMessage());
        }
    }

    private void loadProductToDb(List<FakeProductResponse> fakeProductResponse) {
        List<Product> products = new ArrayList<>();

        for (FakeProductResponse product : fakeProductResponse) {
            products.add(entityMapper.productDtoToProduct(ProductRequestDto.builder()
                    .name(product.getTitle())
                    .price(product.getPrice())
                    .description(product.getDescription())
                    .category(product.getCategory())
                    .imageUrl(product.getImage())
                    .unitInStock(500)
                    .manufacturer("Fake Store Api")
                    .build())
            );
        }
        productDao.saveAll(products);
    }
}
