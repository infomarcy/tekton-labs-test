package com.tekton.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tekton.dao.ProductDao;
import com.tekton.domain.Product;
import com.tekton.model.error.CustomErrorCode;
import com.tekton.model.error.ResponseError;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;

@UtilityClass
@Slf4j
public class CommonUtility {



    private static final String APP_DATE_TIME_FORMAT = "yyyyMMdd";
    private static final String FILE_EXTENSION = ".txt";
    private static final String FILE_START_NAME = "app-response-time-";
    private static final String LOCAL_DIR = System.getProperty("user.dir").concat("/trace-files");


    // Get current date
    public static String getDate(String dateFormat) {
        SimpleDateFormat headerFormat = new SimpleDateFormat(dateFormat, Locale.US);
        headerFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Calendar now = Calendar.getInstance();
        now.add(Calendar.DATE, 0);
        return headerFormat.format(now.getTime());
    }


    public static <T> T fromJSON(Class<?> myClass, final String jsonPacket) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(jsonPacket,
                    objectMapper.getTypeFactory().constructCollectionType(List.class, myClass));
        } catch (Exception e) {
            log.error("error... {}", e.getStackTrace().toString());

        }
        return null;

    }

    public static <T> T jsonAObjeto(String resultado, Class<T> objeto) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readValue(resultado, objeto);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }


    public static void monitorResponseTime(String data) {
        // Check if there is a file created for that date
        List<String> list = new ArrayList<>();
        try {
            if(new File(LOCAL_DIR, getFileName()).exists()){
                List<String> lines = Files.readAllLines(Paths.get(LOCAL_DIR, getFileName()), StandardCharsets.UTF_8);
                for(String line: lines){
                    list.add(line);
                }
            }
            // Add new Data to list
            list.add(data);
            // Write the data to the file
            writeFile(list);
        } catch (Exception e) {
            log.info("There was an error while monitoring the response time {}", e.getMessage());
        }
    }


    private void writeFile(List<String> list) {
        File file = new File(LOCAL_DIR, getFileName());
        StringBuffer chain = new StringBuffer();
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));

            for (String data : list) {
                chain = chain.append(data);
                bufferedWriter.write(chain.toString());
                chain = new StringBuffer();
                bufferedWriter.newLine();
            }
            bufferedWriter.close();
        } catch (IOException e) {
            log.error("{}", e.getMessage());
        }
    }

    private String getFileName() {
        return FILE_START_NAME + getDate(APP_DATE_TIME_FORMAT) + FILE_EXTENSION;
    }
}
