package com.tekton.util;

public class ApiConstant {

    public static final String INTERNAL_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String API_FAKE_PRODUCT_URL = "https://fakestoreapi.com/products";
}
