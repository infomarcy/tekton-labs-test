package com.tekton.util;

import com.tekton.cache.ProductSingleton;
import com.tekton.dao.ProductDao;
import com.tekton.domain.Product;
import com.tekton.model.error.CustomErrorCode;
import com.tekton.model.error.ResponseError;
import com.tekton.model.product.ProductResponseDto;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;


@UtilityClass
@Slf4j
public class ServiceUtils {
    /**
     * fill the ResponseError entity
     *
     * @param description
     * @param code
     * @param httpStatus
     * @return responseError entity
     */
    public ResponseEntity<?> getResponseError(String description, String code, HttpStatus httpStatus) {
        ResponseError responseError = new ResponseError();
        List<String> errors = new ArrayList<>();
        errors.add(description);
        responseError.setErrors(errors);
        responseError.setError_code(code);
        return new ResponseEntity<Object>(responseError, httpStatus);
    }

    public static ResponseEntity<?> getResponseEntity(UUID id, ProductDao productDao, EntityMapper entityMapper) {
        Optional<Product> product = productDao.findById(id);
        if (product.isPresent()) {
            ProductResponseDto foundProduct = entityMapper.productToProductResponseDto(product.get());
            // save poroduct on cache
            ProductSingleton.getInstance().saveProductToCache(foundProduct);
            // send the response
            return new ResponseEntity<>(foundProduct, HttpStatus.OK);
        } else {
            return getResponseError(CustomErrorCode.RESOURCE_NOT_FOUND.getDescription(), CustomErrorCode.RESOURCE_NOT_FOUND.getCode(), HttpStatus.NOT_FOUND);
        }
    }

}
