package com.tekton.util;

import com.tekton.domain.Product;
import com.tekton.model.product.ProductRequestDto;
import com.tekton.model.product.ProductResponseDto;
import org.mapstruct.Mapper;

@Mapper
public interface EntityMapper {
    ProductResponseDto productToProductResponseDto(Product product);
    Product productDtoToProduct(ProductRequestDto productDto);
}
