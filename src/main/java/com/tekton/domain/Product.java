package com.tekton.domain;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class Product extends BaseEntity {
    private String name;
    private BigDecimal price;
    @Column(columnDefinition = "text")
    private String description;
    private String category;
    private String imageUrl;
    private int unitInStock;
    private String manufacturer;

    @Override
    public String toString() {
        String respuesta = null;
        ObjectMapper mapper = new ObjectMapper();
        try {
            respuesta = mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return respuesta;
    }

}