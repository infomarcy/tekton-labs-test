package com.tekton.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

@Slf4j
@Repository
public class CallApiDao {

    private final RestTemplate restTemplate;

    public CallApiDao(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public ResponseEntity<String> callApi(String url) {
        ResponseEntity<String> apiResponse = null;
        boolean retry = false;
        int count = 0;
        do {
            try {
                //Call API
                URI uri = new URI(url);
                log.info("Api URL: {}", uri.toString());
                apiResponse = restTemplate.exchange(uri.toString(), HttpMethod.GET, null, String.class, new Object[0]);

                log.info("Response = Status Code: {} - Body: {}", apiResponse.getStatusCode(), apiResponse.getBody());
                retry = false;
            } catch (HttpClientErrorException e) {
                log.error("Api Fixer::Response = Status Code: {} - Body: {}", e.getStatusCode(), e.getResponseBodyAsString());
                retry = true;
                ++count;
            } catch (Exception e) {
                log.error("Error: " + e.getMessage());
            }
        } while (retry && count <= 1);
        if (apiResponse == null) {
            return null;
        }
        return apiResponse;
    }


}
