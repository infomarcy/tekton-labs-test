package com.tekton.dao;

import com.tekton.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface ProductDao extends JpaRepository<Product, UUID> {
    Optional<Product> findById(UUID id);
}
