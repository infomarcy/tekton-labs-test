package com.tekton.service;

import com.tekton.model.product.ProductRequestDto;
import org.springframework.http.ResponseEntity;

import java.util.UUID;

public interface ProductService {

    ResponseEntity<?> getProductById(UUID id);

    ResponseEntity<?> saveProduct(ProductRequestDto request);

    ResponseEntity<?> updateProduct(ProductRequestDto request, UUID id);
}
