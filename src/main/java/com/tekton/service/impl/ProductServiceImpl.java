package com.tekton.service.impl;

import com.tekton.cache.ProductSingleton;
import com.tekton.dao.ProductDao;
import com.tekton.domain.Product;
import com.tekton.model.product.ProductRequestDto;
import com.tekton.model.error.CustomErrorCode;
import com.tekton.model.product.ProductResponseDto;
import com.tekton.service.ProductService;
import com.tekton.util.EntityMapper;
import com.tekton.util.ServiceUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductDao productDao;
    private final EntityMapper entityMapper;

    @Override
    public ResponseEntity<?> getProductById(UUID id) {
       return ProductSingleton.getInstance().getProductById(id, productDao, entityMapper);
    }


    @Override
    public ResponseEntity<?> saveProduct(ProductRequestDto request) {
        try {
            return new ResponseEntity<>(entityMapper.productToProductResponseDto(productDao.save(entityMapper.productDtoToProduct(request))), HttpStatus.CREATED);
        } catch (Exception e) {
            return ServiceUtils.getResponseError(String.format(CustomErrorCode.UNKNOWN_ERROR.getDescription(), e.getMessage()), CustomErrorCode.UNKNOWN_ERROR.getCode(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<?> updateProduct(ProductRequestDto request, UUID id) {
        try {
            // Get a product by Id
            Optional<Product> product = productDao.findById(id);
            if (product.isPresent()) {

                Product updateProduct  = entityMapper.productDtoToProduct(request);
                updateProduct.setId(id);

                ProductResponseDto productResponseDto = entityMapper.productToProductResponseDto(productDao.save(updateProduct));
                //If the product is in the cache update it
                if(ProductSingleton.getInstance().isProductInCache(id)){
                    ProductSingleton.getInstance().saveProductToCache(productResponseDto);
                }
                return new ResponseEntity<>(productResponseDto, HttpStatus.OK);

            } else {
                return ServiceUtils.getResponseError(CustomErrorCode.RESOURCE_NOT_FOUND.getDescription(), CustomErrorCode.RESOURCE_NOT_FOUND.getCode(), HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return ServiceUtils.getResponseError(String.format(CustomErrorCode.UNKNOWN_ERROR.getDescription(), e.getMessage()), CustomErrorCode.UNKNOWN_ERROR.getCode(), HttpStatus.BAD_REQUEST);
        }
    }


}
