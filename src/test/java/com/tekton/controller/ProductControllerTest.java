package com.tekton.controller;

import com.tekton.cache.ProductSingleton;
import com.tekton.dao.ProductDao;
import com.tekton.domain.Product;
import com.tekton.model.product.ProductRequestDto;
import com.tekton.model.product.ProductResponseDto;
import com.tekton.service.ProductService;
import com.tekton.util.EntityMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.core.Is.is;


@ExtendWith(MockitoExtension.class)
class ProductControllerTest {

    @Mock
    ProductDao productDao;

    @Mock
    ProductService productService;

    @InjectMocks
    ProductController productController;


    MockMvc mockMvc;

    ProductRequestDto productRequestDto;
    ProductResponseDto productResponseDto;
    ResponseEntity<?> responseEntity;
    Product product;

    @BeforeEach
    void setup() {

        product = new Product();
        product.setId(UUID.randomUUID());
        product.setName("Tomato");
        product.setPrice(new BigDecimal(0.66));
        product.setDescription("Red and fresh");
        product.setCategory("Organic");
        product.setManufacturer("Tillman Tomatoes");
        product.setImageUrl("https://tomatos-tlalpan/1642482/red-tomato.jpg");
        product.setUnitInStock(250);

        productRequestDto = ProductRequestDto.builder().name("Tomato")
                .price(new BigDecimal(0.66))
                .description("Red and fresh")
                .category("Organic")
                .manufacturer("Tillman Tomatoes")
                .imageUrl("https://tomatos-tlalpan/1642482/red-tomato.jpg")
                .unitInStock(250)
                .build();


        mockMvc = MockMvcBuilders.standaloneSetup(productController).build();
    }

    @Test
    void getProductById() throws Exception {
        Mockito.when(productService.getProductById(any())).thenReturn(new ResponseEntity(productResponseDto, HttpStatus.OK));

        mockMvc.perform(get("/api/v1/products/" + product.getId()))
                .andExpect(status().isOk());
    }

}