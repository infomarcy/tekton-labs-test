package com.tekton.service;

import com.tekton.dao.ProductDao;
import com.tekton.domain.Product;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;


import java.math.BigDecimal;
import java.util.UUID;


class ProductServiceIT {

    @Mock
    private ProductDao productRepository;

    private Product product;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);

        product = new Product();
        product.setId(UUID.randomUUID());
        product.setName("Tomato");
        product.setPrice(new BigDecimal(0.66));
        product.setDescription("Red and fresh");
        product.setCategory("Organic");
        product.setManufacturer("Tillman Tomatoes");
        product.setImageUrl("https://tomatos-tlalpan/1642482/red-tomato.jpg");
        product.setUnitInStock(250);

    }

    @Test
    void saveProduct() {
        //given
        given(productRepository.save(any(Product.class))).willReturn(product);
        //when
        Product response = productRepository.save(product);
        //then
        then(productRepository).should().save(any(Product.class));
        assertNotNull(response);

        assertEquals(product, response);

    }
}