# Tekton Labs Hiring Test
Api to manage products 

## Build Project
1. Open project
2. Go to the root of the project  and run:
   ```mvn clean verify ```

3. Run project on `dev` environment using command line : ```mvn spring-boot:run```

4. Run project on `prod` environment using command line : ```mvn spring-boot:run -Dspring-boot.run.profiles=prod```

For the `prod` profile a MySQL database is required   
`Run MySQL in a docker container command`
```
docker run -d \
--name consulvex_mysql_db \
-e MYSQL_ROOT_PASSWORD=password1 \
-e MYSQL_DATABASE=consulvex_db \
-e MYSQL_USER=infomarcy \
-e MYSQL_PASSWORD=password2 \
-p 3306:3306 \
mysql:latest
```

5. Use: [This Swagger Documentation Link](http://localhost:8080/swagger-ui.html#) to test project endpoints.

## Database
1. For the Dev environment is using an `H2` Database
2. For the Prod environment is using an `MySQL` Database

## Load Product
When the project start the products are being loaded in the database from the API: [https://fakestoreapi.com/products](https://fakestoreapi.com/products) in the class `com.tekton.boostrap.LoadDefaultData`

## Log response time
The response time of the services will be saved on the directory `~/tekton/trace-files`

The response time is being log using Spring aspect oriented programming(AOP) in the class  `com.tekton.aop.LoggerAspect`

## Health check
[http://localhost:8080/actuator/health](http://localhost:8080/actuator/health)

## Caching Data
1. When `getProductById` method is called  the data is saved on memory in a singleton.
2. When a product is updated, first we check if the product is on the cache and if it is there the data for that specific product in the cache is updated as well 
3. Implementation will be found in the class `com.tekton.cache.ProductSingleton`